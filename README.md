This is an archival project that has not been recently tested or
maintained. Sorry about that. It did a [bunch of different
things](https://bitbucket.org/lwaldron/survhd/downloads/survHD_vignette.pdf),
making it more difficult to maintain than it should have been. It was
2012, and that's hindsight.

At least, its most useful method
([mas-o-menos](https://bitbucket.org/lwaldron/survhd/downloads/survHD_vignette.pdf))
is very simple, and you can probably add and subtract predictor values
yourself. For information on the method:

Sihai Dave Zhao, Giovanni Parmigiani, Curtis Huttenhower, Levi
Waldron, Mas-o-menos: a simple sign averaging method for
discrimination in genomic data analysis, Bioinformatics, Volume 30,
Issue 21, 1 November 2014, Pages 3062–3069,
https://doi.org/10.1093/bioinformatics/btu488


