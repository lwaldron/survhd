\name{getBestParameters}
\alias{getBestParameters}


\title{
Finds the best tuning parameter 
}

\description{
Given an object of class TuneOut getBestParameters finds the optimal tuning parameter for the specified
resampling iteration.
}

\usage{
getBestParameters(tuneres, res.ind, measure,...)

}

\arguments{
  \item{tuneres}{
an object of class TuneOut which has been created by the function \code{tune}
}

\item{res.ind}{
the resampling iteration for which the optimal tuning-parameter shall be found
}
  \item{measure}{the evaluation criterion 
   
}
\item{\dots}{further arguments to be passed to \code{evaluationsurv}}
}



\value{
a list of the index of the best method and the performances of all tuning parameter values
}


\author{

Christoph Bernau \email{bernau@ibe.med.uni-muenchen.de}
Levi Waldron \email{lwaldron@hsph.harvard.edu}
}

\seealso{
\code{\link{tune}}
}

\examples{
\dontrun{
	set.seed(20)
	nsamples <- 100
	X <- matrix(rnorm(nsamples*10),nrow=nsamples)
	colnames(X) <- make.names(1:ncol(X))
	rownames(X) <- make.names(1:nrow(X))
	time <- rexp(nsamples)
	cens <- sample(0:1,size=nsamples,replace=TRUE)
	y <- Surv(time,cens)
	##bootds defines outer fold:
	LearningSets <- generateLearningsets(y = y, method = "CV", fold = 5, niter = 1, strat = TRUE)
	
tuneres<-tune(X=X,y=y,LearningSets=LearningSets,survmethod='penalizedSurv')






tuneres2<-tune(X=X,y=y,LearningSets=LearningSets,measure='CvPLogL',survmethod='penalizedSurv')

getBestParameters(tuneres,1,measure='HarrellC')

getBestParameters(tuneres2,1)
}

}

\keyword{survival}