\name{internals}
\alias{plotprob}
\alias{roundvector}
\alias{rowswaps}
\alias{ROCinternal}
\alias{bklr}
\alias{bkreg}
\alias{bklr.predict}
\alias{care.exp}
\alias{care.dev}
\alias{SuperPcSurv-class}
\alias{UniCoxSurv-class}
\alias{MeasureC-class}
\alias{mklr}
\alias{mkreg}
\alias{mklr.predict}
\alias{my.care.exp}
\alias{characterplot}
\alias{safeexp}
\alias{FUN_logplik}
\title{Internal functions}
\description{internal function not intended to be called directly by the user.}
\keyword{multivariate}
