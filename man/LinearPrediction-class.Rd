\name{LinearPrediction-class}
\docType{class}
\alias{lp<-}
\alias{lp}
\alias{lp,LinearPrediction-method}
\alias{lp<-,LinearPrediction,numeric-method}
\alias{LinearPrediction-class}
\alias{LinearPrediction}


\title{"LinearPrediction"}
\description{Object returned by \code{\link{predict}}-functions in survHD if
a linear predictor is to be estimated.}
\section{Slots}{
	 \describe{
    \item{\code{lp}}{Object of class \code{numeric} representing the linear
    predictor.}  
  }
}

\section{Accessor and replacement functions}{
The accessor and replacement functions are named after the slot. 
For the slot \code{lp} we have: 

\describe{
\item{accessor function:}{\code{lp(LinearPrediction-object)}}
\item{replacement function:}{\code{lp(LinearPrediction-object)<-}}
}
}


\author{Christoph Bernau \email{bernau@ibe.med.uni-muenchen.de},
Levi Waldron \email{lwaldron@hsph.harvard.edu},
        Anne-Laure Boulesteix \email{boulesteix@ibe.med.uni-muenchen.de}}

\seealso{
        \code{\link{predict}} }
\keyword{multivariate}
