\name{MeasureOut-class}
\docType{class}
\alias{MeasureOut-class}
\alias{MeasureOut}
\alias{conf.int<-}
\alias{conf.int}
\alias{conf.int,MeasureOut-method}
\alias{conf.int<-,MeasureOut,numeric-method}
\alias{estimate<-}
\alias{estimate}
\alias{estimate,MeasureOut-method}
\alias{estimate<-,MeasureOut,numeric-method}



\title{"MeasureOut"}
\description{Object returned by \code{\link{compare}}}
\section{Slots}{
	 \describe{
    \item{\code{estimate}}{point estimate for the comparison criterion}
    \item{\code{conf.int}}{confidence interval of the comparison criterion}
    
  }
}

\section{Accessor and replacement functions}{
The accessor and replacement functions are named after the slots. For example,
for the slot \code{estimate} we have: 

\describe{
\item{accessor function:}{\code{estimate(MeasureOut-object)}}
\item{replacement function:}{\code{estimate(MeasureOut-object)<-}}
}
}


\author{Christoph Bernau \email{bernau@ibe.med.uni-muenchen.de},
Levi Waldron \email{lwaldron@hsph.harvard.edu},
        Anne-Laure Boulesteix \email{boulesteix@ibe.med.uni-muenchen.de}}

\seealso{
        \code{\link{ModelLearned}} }
\keyword{multivariate}