\name{fastCox}
\alias{fastCox}
\title{Filter function for Gene Selection}
\description{The functions listed above are usually not called by the
             user but via \code{\link{geneSelection}}.}
\usage{
fastCox(X,y,learnind,criterion,...)
}
\arguments{
 \item{X}{A \code{numeric} matrix of gene expression values.}
 \item{y}{A \code{numeric} vector of class labels.}
 \item{learnind}{An index vector specifying the observations that
                  belong to the learning set.}
 \item{criterion}{can be either 'pvalue' or 'coefficient' and determines which of these two criteria shall be returned by \code{method} }
 \item{...}{Currently unused argument.}}
                  
\value{An object of class \code{\link{VarSelOut}}.}
\references{
Cox, D. R. (1972) Regression models and life tables (with discussion), Journal of the Royal Statistical Society,
Series B, 34, 187-220 
}
\examples{
set.seed(2)
nsamples <- 100
X <- matrix(rnorm(nsamples*200),nrow=nsamples)
colnames(X) <- make.names(1:ncol(X))
rownames(X) <- make.names(1:nrow(X))
time <- rexp(nsamples)
cens <- sample(0:1,size=nsamples,replace=TRUE)
y <- Surv(time,cens)
learnind<-1:65
varseloutraw<-fastCox(X,y,learnind,criterion='pvalue')
}
\keyword{multivariate}
