\name{GeneSel-class}
\docType{class}
\alias{GeneSel-class}
\alias{GeneSel}
\alias{show,GeneSel-method}
\alias{rankings<-}
\alias{rankings}
\alias{rankings,GeneSel-method}
\alias{rankings<-,GeneSel,list-method}
\alias{importance<-}
\alias{importance}
\alias{importance,GeneSel-method}
\alias{importance<-,GeneSel,list-method}

\title{"GeneSel"}
\description{Object returned from a call to \code{\link{geneSelection}}}
\section{Slots}{
	 \describe{
    \item{\code{rankings}:}{A list of matrices (usually of length one).
                             Each list
                            element is a \code{matrix} with rows corresponding  to
                            iterations (different \code{LearningSets}) and columns
                            to variables.
                            Each row thus contains an index vector representing the order of the
                            variables with respect to their variable importance
                            (s. slot \code{importance})}
    \item{\code{importance}:}{A list of matrices, with the same structure as
                              described for the slot \code{rankings}.
                              Each row of these matrices are ordered according to
                              \code{rankings} and contain the variable importance
                              measure (absolute value of test statistic or regression
                              coefficient).}
    \item{\code{method}:}{Name of the method used for variable selection, s. \code{\link{geneSelection}}.}
    \item{\code{criterion}:}{the criterion (pvalue or coefficient) used in slot \code{importance}}.
                
                
  }
}
\section{Methods}{
  \describe{
    \item{show}{Use \code{show(GeneSel-object)} for brief information}
    \item{toplist}{Use \code{toplist(GeneSel-object, k=10, iter = 1)} to display
                   the top first 10 variables and their variable importance
                   for the first iteration (first learningset).}
    \item{plot}{Use \code{plot(GeneSel-object, k=10, iter=1)} to display
                a barplot of the variable importance of the top first 10
                variables, s. \code{\link{plot,GeneSel-method}}}
	 }
}



\section{Accessor and replacement functions}{
The accessor and replacement functions are named after the slots. For example,
for the slot \code{rankings} we have: 

\describe{
\item{accessor function:}{\code{rankings(GeneSel-object)}}
\item{replacement function:}{\code{rankings(GeneSel-object)<-}}
}
}

\section{Constructor}{
Objects of class \code{GeneSel} can be constructed using the constructor 
\code{GeneSel} which accepts arguments identical to the slots described in
Section \code{Slots}.
}

\author{Christoph Bernau \email{bernau@ibe.med.uni-muenchen.de}
Martin Slawski \email{ms@cs.uni-sb.de}

        Anne-Laure Boulesteix \email{boulesteix@ibe.med.uni-muenchen.de}}

\seealso{\code{\link{geneSelection}}}
\keyword{multivariate}
